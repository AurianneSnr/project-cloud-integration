package com.example.demo;

import java.io.Serializable;

public class Drink implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean alcoholed;

    private int shots;

    private DrinkType drinkType;

    private int orderNumber;

    // Default constructor required by Jackson Java JSON-processor
    public Drink() {}

    public Drink(int orderNumber, DrinkType drinkType, boolean alcoholed, int shots) {
        this.orderNumber = orderNumber;
        this.drinkType = drinkType;
        this.alcoholed = alcoholed;
        this.shots = shots;
    }


    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public boolean isAlcoholed() {
        return alcoholed;
    }

    public void setAlcoholed(boolean alcoholed) {
        this.alcoholed = alcoholed;
    }

    public DrinkType getDrinkType() {
        return this.drinkType;
    }

    public void setDrinkType(DrinkType drinkType) {
        this.drinkType = drinkType;
    }

    public int getShots() {
        return this.shots;
    }

    public void setShots(int shots) {
        this.shots = shots;
    }

    @Override
    public String toString() {
        return (alcoholed?"Alcoholic":"Virgin")  + " " + drinkType.toString() + ", " + shots + " shots.";
    }
}
