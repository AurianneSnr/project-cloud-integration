package com.example.demo;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BarApp {
    public static void main(String[] args) {
        AbstractApplicationContext context =
                new ClassPathXmlApplicationContext("/BarApp-xml.xml", BarApp.class);

        Bar bar = (Bar) context.getBean("cafe");
        for (int i = 1; i <= 100; i++) {
            Order order = new Order(i);
            order.addItem(DrinkType.BEER, 1, false);
            order.addItem(DrinkType.SUNRISE, 3, true);
            order.addItem(DrinkType.MOJITO, 3, true);
            bar.placeOrder(order);
        }
        context.close();
    }
}
