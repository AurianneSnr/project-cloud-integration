package com.example.demo;

import java.io.Serializable;

public class OrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private DrinkType type;

    private int shots = 1;

    private boolean alcoholed = false;

    /** the order this item is tied to */
    private int orderNumber;

    // Default constructor required by Jackson Java JSON-processor
    public OrderItem() {}

    public OrderItem(int orderNumber, DrinkType type, int shots, boolean alcoholed) {
        this.orderNumber = orderNumber;
        this.type = type;
        this.shots = shots;
        this.alcoholed = alcoholed;
    }

    public int getOrderNumber() {
        return this.orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public boolean isAlcoholed() {
        return alcoholed;
    }

    public void setAlcoholed(boolean alcoholed) {
        this.alcoholed = alcoholed;
    }

    public int getShots() {
        return shots;
    }

    public void setShots(int shots) {
        this.shots = shots;
    }

    public DrinkType getDrinkType() {
        return this.type;
    }

    public void setDrinkType(DrinkType type) {
        this.type = type;
    }

    public String toString() {
        return ((this.alcoholed)? "Alcoholic":"Virgin") + this.shots + " shot " + this.type;
    }
}
