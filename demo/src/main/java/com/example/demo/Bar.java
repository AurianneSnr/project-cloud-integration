package com.example.demo;

import org.springframework.integration.annotation.Gateway;

public interface Bar {
    @Gateway(requestChannel="orders")
    void placeOrder(Order order);
}
