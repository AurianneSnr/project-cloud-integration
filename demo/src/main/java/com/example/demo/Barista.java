package com.example.demo;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
public class Barista {
    private static Log logger = LogFactory.getLog(Barista.class);

    private long softDrinkDelay = 1000;

    private long alcoholedDrinkDelay = 5000;

    private final AtomicInteger softDrinkCounter = new AtomicInteger();

    private final AtomicInteger alcoholedDrinkCounter = new AtomicInteger();


    

    public Drink prepareSoftDrink(OrderItem orderItem) {
        try {
            Thread.sleep(this.softDrinkDelay);
            logger.info(Thread.currentThread().getName()
                    + " prepared hot drink #" + softDrinkCounter.incrementAndGet() + " for order #"
                    + orderItem.getOrderNumber() + ": " + orderItem);
            return new Drink(orderItem.getOrderNumber(), orderItem.getDrinkType(), orderItem.isAlcoholed(),
                    orderItem.getShots());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return null;
        }
    }

    public Drink prepareAlcoholedDrink(OrderItem orderItem) {
        try {
            Thread.sleep(this.alcoholedDrinkDelay);
            logger.info(Thread.currentThread().getName()
                    + " prepared cold drink #" + alcoholedDrinkCounter.incrementAndGet() + " for order #"
                    + orderItem.getOrderNumber() + ": " + orderItem);
            return new Drink(orderItem.getOrderNumber(), orderItem.getDrinkType(), orderItem.isAlcoholed(),
                    orderItem.getShots());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return null;
        }
    }
}
