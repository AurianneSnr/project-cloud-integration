package com.example.demo;

import java.io.Serializable;

public enum DrinkType implements Serializable {

    MOJITO,
    BEER,
    PINA_COLADA,
    SUNRISE

}